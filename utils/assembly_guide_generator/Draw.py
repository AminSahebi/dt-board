#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 UCSD Bespoken Systems Group
# All rights reserved
# Created by Shengye Wang <shengye@ucsd.edu>

import time
from PIL import Image, ImageDraw, ImageFont
DOT_SIZE = 20
ACTUAL_WIDTH = 162.2
ACTUAL_HEIGHT = 71.2
DEFAULT_FONT = ImageFont.truetype(font=None, size=20, index=0, encoding='', filename='Consolas.ttf')
HEIGHT_EXPAND = 8.5 / 11.0
BOARDER_EXPAND_RATIO = 1.1
TOOL_VERSION = 'UCSD BSG Assemble Guide Generator V1.0'

plot_info = TOOL_VERSION

img = Image.new("RGB", (512, 512), "white")
draw = ImageDraw.Draw(img)
img_width, img_height = img.size

template = {'TOP':
                {   'template_file_name' : "Templates/TOP.jpg",
                    'x_inv' : False,
                    'bgcolor' : "DeepSkyBlue",
                    'fgcolor' : 'DarkBlue',
                    'titlecolor' : 'Blue',
                    'part_font' : ImageFont.truetype(font=None, size=20, index=0, encoding='', filename='Consolas.ttf'),
                    'title_font' : ImageFont.truetype(font=None, size=250, index=0, encoding='', filename='Consolas.ttf'),
                    'description_font' : ImageFont.truetype(font=None, size=50, index=0, encoding='', filename='Consolas.ttf')
                },
            'BOT':
                {   'template_file_name' : "Templates/BOT.jpg",
                    'x_inv' : True,
                    'bgcolor' : "HotPink",
                    'fgcolor' : 'Maroon',
                    'titlecolor' : 'Red',
                    'part_font' : ImageFont.truetype(font=None, size=20, index=0, encoding='', filename='Consolas.ttf'),
                    'title_font' : ImageFont.truetype(font=None, size=250, index=0, encoding='', filename='Consolas.ttf'),
                    'description_font' : ImageFont.truetype(font=None, size=50, index=0, encoding='', filename='Consolas.ttf')
                }
            }

def board_draw_comp(tmplt, x, y, text = ""):
    font = template[tmplt]['part_font'];
    fgcolor = template[tmplt]['fgcolor'];
    bgcolor = template[tmplt]['bgcolor'];
    x = float(x) / ACTUAL_WIDTH * img_width + img_width
    if (template[tmplt]['x_inv']):
        x = img_width - x
    y = img_height - float(y) / ACTUAL_HEIGHT * img_height
    draw.chord((x - DOT_SIZE, y - DOT_SIZE, x + DOT_SIZE, y + DOT_SIZE), 0, 360, fill=bgcolor)
    text_width, text_height = draw.textsize(text, font=font)
    draw.text((x - text_width / 2, y - text_height / 2), text, fill=fgcolor, font=font, anchor=None)

cur_desc = ''
cur_temp = ''
cur_name = ''
cur_title = ''

fp = open('Draw_Config.txt')
lines = fp.readlines()
fp.close()

i = 0
while (i < len(lines)):
    line = lines[i].strip()
    if line.startswith('PLOT_INFO'):
        plot_info = line[len('PLOT_INFO'):].strip()
    if line.startswith('GROUP'):
        grp_data = line.split()
        cur_name = grp_data[1]
        cur_title = cur_name
        cur_temp = grp_data[2]
        img = Image.open(template[cur_temp]['template_file_name'])
        draw = ImageDraw.Draw(img)
        img_width, img_height = img.size
        print "Group %s" % cur_name
    if line.startswith('TITLE'):
        cur_title = line[len('TITLE'):].strip()
    if line == 'DESCRIPTION':
        i += 1
        cur_desc = []
        while (lines[i].strip() != 'END_DESCRIPTION'):
            cur_desc.append(lines[i].strip())
            i += 1
    if line == 'PARTS':
        i += 1
        while (lines[i].strip() != 'END_PARTS'):
            line = lines[i].strip()
            if line.find('#') >= 0:
                line = line[0: line.find('#')].strip()
            if line == '':
                continue
            prt_data = line.strip().split()
            i += 1
            board_draw_comp(cur_temp, float(prt_data[1]), float(prt_data[2]), prt_data[0])
    if line == 'END_GROUP':
        new_width = img_width
        new_height = int(img_width * HEIGHT_EXPAND) # Letter size
        new_img = Image.new("RGB", (new_width, new_height), "white")
        draw = ImageDraw.Draw(new_img)
        draw.text((0, 0), plot_info, fill=template[cur_temp]['titlecolor'], font=template[cur_temp]['description_font'], anchor=None)
        below_plot_info_offset = draw.textsize(plot_info, font=template[cur_temp]['description_font'])[1] * 3 / 2
        draw.text((0, below_plot_info_offset), cur_title, fill=template[cur_temp]['titlecolor'], font=template[cur_temp]['title_font'], anchor=None)
        if (len(cur_desc)):
            text_width = 0
            text_height_offset = below_plot_info_offset
            for desc_line in cur_desc:
                desc_line_width = draw.textsize(desc_line, font=template[cur_temp]['description_font'])[0]
                if (desc_line_width > text_width):
                    text_width = desc_line_width
            for desc_line in cur_desc:
                draw.text((new_width - text_width, text_height_offset), desc_line, fill=template[cur_temp]['titlecolor'], font=template[cur_temp]['description_font'], anchor=None)
                text_height_offset += draw.textsize(desc_line, font=template[cur_temp]['description_font'])[1]
        new_img.paste(img, (0, (new_height - img_height)*2/3))
        time_info = time.strftime("%Y-%m-%d %H:%M")
        footer_height = draw.textsize(TOOL_VERSION, font=template[cur_temp]['description_font'])[1]
        draw.text((0, new_height - footer_height), TOOL_VERSION, fill=template[cur_temp]['titlecolor'], font=template[cur_temp]['description_font'], anchor=None)
        time_info = time.strftime("%Y-%m-%d %H:%M")
        time_width, time_height = draw.textsize(time_info, font=template[cur_temp]['description_font'])
        draw.text((new_width - time_width, new_height - time_height), time_info, fill=template[cur_temp]['titlecolor'], font=template[cur_temp]['description_font'], anchor=None)

        border_expand_img = Image.new("RGB", (int(new_width * BOARDER_EXPAND_RATIO), int(new_height * BOARDER_EXPAND_RATIO)), "white")
        border_expand_img.paste(new_img, (int(((BOARDER_EXPAND_RATIO - 1) / 2.0) * new_width), int(((BOARDER_EXPAND_RATIO - 1) / 2.0) * new_height)))
        border_expand_img.save('Result/%s.jpg' % (cur_name), quality=100)
        print "    Saved as %s" % ('Result/%s.jpg' % (cur_name))
    i += 1

