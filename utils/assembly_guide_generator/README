************************************************************
*               PCB Assembly Guide Generator               *
************************************************************

1) Purpose of the Tool
    This tool print assembly guide for PCB.
    Given a PCB picture and part positions, it put dots on the picture where the part should be.
    Many identical parts can be printed on the same output picture.
    There are two parts of the tool:
        - GenConf.py generates Draw_Config.txt from Parts_XY.txt
          Draw_Config.txt specifies how the output should be printed.
        - Draw.py takes Draw_Config.txt and generate pictures.

2) Usage
    a. GenConf.py
        Put Parts_XY.txt in the current directory and run the script, it will generate Draw_Config.txt
    b. Manually modify Draw_Config.txt
        Draw_Config.txt contains a few groups. Each group will become an output picture.
        GenConf.py only merges the same parts to a group, but we may want to print various parts on the same paper.
        Therefore, it might be useful to manually modify the Draw_Config.txt
        Draw_Config.txt has a format as below:

            PLOT_INFO <PLOT_INFORMATION>

            GROUP <OUTPUT_FILE_NAME> <TEMPLATE_IDENTIFIER>
                [TITLE <TITLE_OF_GROUP>]
                DESCRIPTION
                    <DESCRIPTION_TEXT_LINE_1>
                    <DESCRIPTION_TEXT_LINE_2>
                    <...>
                    <DESCRIPTION_TEXT_LINE_N>
                END_DESCRIPTION
                PARTS
                    <PART_1_IDENTIFIER> <X_COORDINATE> <Y_COORDINATE> [# OPTIONAL_COMMENT]
                    <PART_2_IDENTIFIER> <X_COORDINATE> <Y_COORDINATE> [# OPTIONAL_COMMENT]
                    <...>
                    <PART_N_IDENTIFIER> <X_COORDINATE> <Y_COORDINATE> [# OPTIONAL_COMMENT]
                END_PARTS
            END_GROUP

            [MORE_GROUPS...]
    c. Modify Draw.py with template information
        template is a dictionary of template, whose key is template identifier and value is a dictionary as below:
            {'template_file_name'   : "<PATH_TO_BACKGROUND_PICTURE>",
            'x_inv'                 : <INVERTING_X_COORDINATE, True/False>,
            'bgcolor'               : "<BACKGROUND_COLOR_FOR_DOTS>",
            'fgcolor'               : '<FOREGROUND_COLOR_FOR_TEXT_ON_DOT>',
            'titlecolor'            : '<COLOR_FOR_PAGE_TITLE>',
            # modify below parameters if necessary
            'part_font'             : ImageFont.truetype(font=None, size=20, index=0, encoding='', filename='Consolas.ttf'), 
            'title_font'            : ImageFont.truetype(font=None, size=250, index=0, encoding='', filename='Consolas.ttf'),
            'description_font'      : ImageFont.truetype(font=None, size=50, index=0, encoding='', filename='Consolas.ttf') }
        Example:
            {   'template_file_name'    : "Templates/TOP.jpg",
                'x_inv'                 : False,
                'bgcolor'               : "DeepSkyBlue",
                'fgcolor'               : 'DarkBlue',
                'titlecolor'            : 'Blue',
                'part_font'             : ImageFont.truetype(font=None, size=20, index=0, encoding='', filename='Consolas.ttf'),
                'title_font'            : ImageFont.truetype(font=None, size=250, index=0, encoding='', filename='Consolas.ttf'),
                'description_font'      : ImageFont.truetype(font=None, size=50, index=0, encoding='', filename='Consolas.ttf')
            },

    d. Create Result directory and run the Draw.py script, generate picture will be in Result directory

3) Basic Design
    The script analysis the input file and use Python Pillow to draw.

4) Limitations
    Pillow or PIL library must be installed on the machine
    Draw_Config.txt will need some manual review, reordering and clean up.

