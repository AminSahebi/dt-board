#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 UCSD Bespoken Systems Group
# All rights reserved
# Created by Shengye Wang <shengye@ucsd.edu>

def can_be_same_grp(part_a, part_b):
    if (part_a[5] != part_b[5]): # Layer
        return False
    if (part_a[3] != part_b[3]): # Type
        return False
    if (part_a[4] != part_b[4]): # Value
        return False
    return True

output_buffer = []
sole_buffer = {}
def flush_parts_buffer(parts_buffer):
    global output_buffer
    if (len(parts_buffer) == 0):
        return
    if (len(parts_buffer) > 1):
        grp_name = "%s_%s_%s" % (parts_buffer[0][3], parts_buffer[0][4], parts_buffer[0][5])
        grp_name = grp_name.replace("/", "_")
        grp_name = grp_name.replace("@", "_")
        grp_name = grp_name.replace("__", "_")
        grp_title = "%s %s" % (parts_buffer[0][3], parts_buffer[0][4])
        grp_desc = '\n'.join((parts_buffer[0][0], parts_buffer[0][2], parts_buffer[0][3], parts_buffer[0][4], parts_buffer[0][5]))
        grp_temp = parts_buffer[0][5]
        output_buffer.append("GROUP %s %s" % (grp_name, grp_temp))
        output_buffer.append("TITLE %s" % (grp_title))
        output_buffer.append("DESCRIPTION")
        output_buffer += grp_desc.split('\n')
        output_buffer.append("END_DESCRIPTION")
        output_buffer.append("PARTS")
        for item in parts_buffer:
            output_buffer.append("%s %s %s # %s" % (item[1], item[6], item[7], '/'.join(item)))
        output_buffer.append("END_PARTS")
        output_buffer.append("END_GROUP")
        output_buffer.append("")
    else:
        part = parts_buffer[0]
        if not(part[5] in sole_buffer):
            sole_buffer[part[5]] = []
        sole_buffer[part[5]].append(part)

def flush_sole_buffer():
    global output_buffer
    for buffer_layer in sole_buffer:
        parts_buffer = sole_buffer[buffer_layer]
        parts_buffer.sort(key = lambda x: x[1])
        if (len(parts_buffer) == 0):
            return
        grp_name = "MISC_%s" % (buffer_layer)
        grp_name = grp_name.replace("/", "_")
        grp_name = grp_name.replace("@", "_")
        grp_name = grp_name.replace("__", "_")
        grp_title = "MISC %s" % (buffer_layer)
        grp_temp = buffer_layer
        grp_desc = ""
        grp_desc_buffer = []
        for item in parts_buffer:
            grp_desc_buffer.append('%s: %s %s' % (item[1], item[3], item[4]))
        grp_desc = '\n'.join(grp_desc_buffer)
        output_buffer.append("GROUP %s %s" % (grp_name, grp_temp))
        output_buffer.append("TITLE %s" % (grp_title))
        output_buffer.append("DESCRIPTION")
        output_buffer += grp_desc.split('\n')
        output_buffer.append("END_DESCRIPTION")
        output_buffer.append("PARTS")
        for item in parts_buffer:
            output_buffer.append("%s %s %s # %s" % (item[1], item[6], item[7], '/'.join(item)))
        output_buffer.append("END_PARTS")
        output_buffer.append("END_GROUP")
        output_buffer.append("")

fp = open("Parts_XY.txt")
lines = fp.readlines()
fp.close()

parts = []

for line in lines:
    if (line.strip() != ''):
        info = line.strip().split('\t')
        if (info[0] != 'Famliy'):
            parts.append(info)

parts.sort(key = lambda x:(x[4], x[3], x[5], x[1]))

parts_buffer = []
for part in parts:
    if (len(parts_buffer) == 0):
        parts_buffer.append(part)
        continue
    if (can_be_same_grp(parts_buffer[0], part)):
        parts_buffer.append(part)
    else:
        flush_parts_buffer(parts_buffer)
        parts_buffer = [part]

if (len(parts_buffer)):
    flush_parts_buffer(parts_buffer)
flush_sole_buffer()

fp = open("Draw_Config.txt", 'w')
fp.write('\n'.join(output_buffer))
fp.close()

