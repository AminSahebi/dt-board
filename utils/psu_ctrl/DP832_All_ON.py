#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 UCSD Bespoken Systems Group
# All rights reserved
# Created by Shengye Wang <shengye@ucsd.edu>

import visa
rm = visa.ResourceManager()
rm.list_resources()
inst = rm.open_resource('USB0::0x1AB1::0x0E11::DP8C161850666::INSTR')
print(inst.query("*IDN?"))
inst.write(":OUTP ALL,ON")
