#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 UCSD Bespoken Systems Group
# All rights reserved
# Created by Shengye Wang <shengye@ucsd.edu>

# ----------------------------------- Please modify below -----------------------------------
a_filename = 'Gateway_FPGA.ucf'
b_filename = 'gateway_top.ucf'
# ----------------------------------- Please modify above -----------------------------------

import re

def print_columns(lines, spacing = 4):
    widths = [max(len(value) for value in column) + spacing for column in zip(*lines)]
    rtn = ''
    for line in lines:
        rtn += ("%s\n" % (''.join('%-*s|    ' % item for item in zip(widths, line)).strip()))
    return rtn

a_map = {}
b_map = {}

def make_map(filename):
    fp = open(filename)
    lines = fp.readlines()
    fp.close()
    result = {}
    for l in lines:
        remat = re.match(r'NET\s*"(?P<net>\w+)"\s*LOC\s*=\s*"(?P<loc>\w+)"', l.strip())
        if (remat):
            matdict = remat.groupdict()
            result[matdict['loc']] = (matdict['net'], l.strip())
    return result

a_map = make_map(a_filename)
b_map = make_map(b_filename)

merge_map = a_map.copy()
merge_map.update(b_map)

outlist = []
for loc in merge_map:
    a_sig = ''
    b_sig = ''
    a_rec = ''
    b_rec = ''
    if (loc in a_map):
        a_sig = a_map[loc][0]
        a_rec = a_map[loc][1]
    if (loc in b_map):
        b_sig = b_map[loc][0]
        b_rec = b_map[loc][1]
    outlist.append((loc, a_sig, b_sig, a_rec, b_rec))
outlist.sort(key=lambda x:x[2])
outlist.sort(key=lambda x:x[1])
outlist.insert(0, ('LOC', 'A_SIG(%s)' % a_filename, 'B_SIG(%s)' % b_filename, 'A_REC(%s)' % a_filename, 'B_REC(%s)' % b_filename))
print print_columns(outlist)

