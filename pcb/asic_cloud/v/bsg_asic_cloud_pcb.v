// bsg_asic_cloud_pcb.v
//
// simulates connectivity of the asic cloud PCB
//
// this is intended as the canonical simulation file
// but currently it may only implement a subset of
// all of the wires and functionality -- please
// extend rather than cloning the file and modifying it
//

`timescale 1ps/1ps

module bsg_asic_cloud_pcb
(
    // this is the FMC connector
    inout [33:00] LAxx_N
    , inout [33:00] LAxx_P      //  e.g. LA14_N
    , inout CLK0_C2M_N
    , inout CLK0_C2M_P
    , inout CLK0_M2C_N
    , inout CLK0_M2C_P

    // SMA connectors (for simulation)
    , input ASIC_SMA_IN_N
    , input ASIC_SMA_IN_P       // terminated on ASIC side
    , inout ASIC_SMA_OUT_N
    , inout ASIC_SMA_OUT_P      // unterminated

    , inout FPGA_SMA_IN_N
    , inout FPGA_SMA_IN_P       // unterminated
    , inout FPGA_SMA_OUT_N
    , inout FPGA_SMA_OUT_P      // unterminated

    // LEDs (for simulation)
    , output [3:0] FPGA_LED     // from GW   FPGA
    , output [1:0] ASIC_LED     // from ASIC FPGA

    , input  UART_RX
    , output UART_TX

    // low-true reset signal for GW FPGA (normal driven by reset controller)
    , input PWR_RSTN
  );


  //
  // Comm link wires (Between GW and IC0)
  //

  wire IC0_GW_CL_CLK;
  wire IC0_GW_CL_V;
  wire IC0_GW_CL_TKN;
  wire IC0_GW_CL_D0;
  wire IC0_GW_CL_D1;
  wire IC0_GW_CL_D2;
  wire IC0_GW_CL_D3;
  wire IC0_GW_CL_D4;
  wire IC0_GW_CL_D5;
  wire IC0_GW_CL_D6;
  wire IC0_GW_CL_D7;
  wire IC0_GW_CL_D8;

  wire GW_IC0_CL_CLK;
  wire GW_IC0_CL_V;
  wire GW_IC0_CL_TKN;
  wire GW_IC0_CL_D0;
  wire GW_IC0_CL_D1;
  wire GW_IC0_CL_D2;
  wire GW_IC0_CL_D3;
  wire GW_IC0_CL_D4;
  wire GW_IC0_CL_D5;
  wire GW_IC0_CL_D6;
  wire GW_IC0_CL_D7;
  wire GW_IC0_CL_D8;
  
  //
  // Comm link wires (Between IC0 and IC1)
  //

  wire IC1_IC0_CL_CLK;
  wire IC1_IC0_CL_V;
  wire IC1_IC0_CL_TKN;
  wire IC1_IC0_CL_D0;
  wire IC1_IC0_CL_D1;
  wire IC1_IC0_CL_D2;
  wire IC1_IC0_CL_D3;
  wire IC1_IC0_CL_D4;
  wire IC1_IC0_CL_D5;
  wire IC1_IC0_CL_D6;
  wire IC1_IC0_CL_D7;
  wire IC1_IC0_CL_D8;

  wire IC0_IC1_CL_CLK;
  wire IC0_IC1_CL_V;
  wire IC0_IC1_CL_TKN;
  wire IC0_IC1_CL_D0;
  wire IC0_IC1_CL_D1;
  wire IC0_IC1_CL_D2;
  wire IC0_IC1_CL_D3;
  wire IC0_IC1_CL_D4;
  wire IC0_IC1_CL_D5;
  wire IC0_IC1_CL_D6;
  wire IC0_IC1_CL_D7;
  wire IC0_IC1_CL_D8;

  //
  // Misc wires (GW)
  //

  wire GW_TAG_CLKO;
  wire GW_TAG_DATAO;
  
  wire GW_IC0_TAG_EN;
  wire GW_IC1_TAG_EN;

  wire GW_CLKA;
  wire GW_CLKB;
  wire GW_CLKC;

  wire GW_SEL0;
  wire GW_SEL1;
  //wire GW_SEL2;

  wire GW_CLK_RESET;
  wire GW_CORE_RESET;

  //
  // DDR wires (IC0)
  //

  wire IC0_DDR_CK_P;
  wire IC0_DDR_CK_N;

  wire IC0_DDR_CKE;

  wire IC0_DDR_CS_N;
  wire IC0_DDR_RAS_N;
  wire IC0_DDR_CAS_N;
  wire IC0_DDR_WE_N;

  wire IC0_DDR_RESET_N;
  wire IC0_DDR_ODT;

  wire IC0_DDR_BA0;
  wire IC0_DDR_BA1;
  wire IC0_DDR_BA2;

  wire IC0_DDR_ADDR0;
  wire IC0_DDR_ADDR1;
  wire IC0_DDR_ADDR2;
  wire IC0_DDR_ADDR3;
  wire IC0_DDR_ADDR4;
  wire IC0_DDR_ADDR5;
  wire IC0_DDR_ADDR6;
  wire IC0_DDR_ADDR7;
  wire IC0_DDR_ADDR8;
  wire IC0_DDR_ADDR9;
  wire IC0_DDR_ADDR10;
  wire IC0_DDR_ADDR11;
  wire IC0_DDR_ADDR12;
  wire IC0_DDR_ADDR13;
  wire IC0_DDR_ADDR14;
  wire IC0_DDR_ADDR15;

  wire IC0_DDR_DM0;
  wire IC0_DDR_DM1;
  wire IC0_DDR_DM2;
  wire IC0_DDR_DM3;

  wire IC0_DDR_DQS0_P;
  wire IC0_DDR_DQS0_N;
  wire IC0_DDR_DQS1_P;
  wire IC0_DDR_DQS1_N;
  wire IC0_DDR_DQS2_P;
  wire IC0_DDR_DQS2_N;
  wire IC0_DDR_DQS3_P;
  wire IC0_DDR_DQS3_N;

  wire IC0_DDR_DQ0;
  wire IC0_DDR_DQ1;
  wire IC0_DDR_DQ2;
  wire IC0_DDR_DQ3;
  wire IC0_DDR_DQ4;
  wire IC0_DDR_DQ5;
  wire IC0_DDR_DQ6;
  wire IC0_DDR_DQ7;
  wire IC0_DDR_DQ8;
  wire IC0_DDR_DQ9;
  wire IC0_DDR_DQ10;
  wire IC0_DDR_DQ11;
  wire IC0_DDR_DQ12;
  wire IC0_DDR_DQ13;
  wire IC0_DDR_DQ14;
  wire IC0_DDR_DQ15;
  wire IC0_DDR_DQ16;
  wire IC0_DDR_DQ17;
  wire IC0_DDR_DQ18;
  wire IC0_DDR_DQ19;
  wire IC0_DDR_DQ20;
  wire IC0_DDR_DQ21;
  wire IC0_DDR_DQ22;
  wire IC0_DDR_DQ23;
  wire IC0_DDR_DQ24;
  wire IC0_DDR_DQ25;
  wire IC0_DDR_DQ26;
  wire IC0_DDR_DQ27;
  wire IC0_DDR_DQ28;
  wire IC0_DDR_DQ29;
  wire IC0_DDR_DQ30;
  wire IC0_DDR_DQ31;

  //
  // Misc wires (IC0)
  //

  wire IC0_CLKO;  // Clock out (for scoping clock generators)

  //
  // GATEWAY SOCKET
  //

  bsg_gateway_socket
    GW
      // IC0 -> GW
      (.CL_IN_CLK_I     (IC0_GW_CL_CLK)
      ,.CL_IN_V_I       (IC0_GW_CL_V)
      ,.CL_IN_TKN_O     (IC0_GW_CL_TKN)
      ,.CL_IN_D0_I      (IC0_GW_CL_D0)
      ,.CL_IN_D1_I      (IC0_GW_CL_D1)
      ,.CL_IN_D2_I      (IC0_GW_CL_D2)
      ,.CL_IN_D3_I      (IC0_GW_CL_D3)
      ,.CL_IN_D4_I      (IC0_GW_CL_D4)
      ,.CL_IN_D5_I      (IC0_GW_CL_D5)
      ,.CL_IN_D6_I      (IC0_GW_CL_D6)
      ,.CL_IN_D7_I      (IC0_GW_CL_D7)
      ,.CL_IN_D8_I      (IC0_GW_CL_D8)

      // IC<n> -> GW
      ,.CL_OUT_CLK_I    ()
      ,.CL_OUT_V_I      ()
      ,.CL_OUT_TKN_O    ()
      ,.CL_OUT_D0_I     ()
      ,.CL_OUT_D1_I     ()
      ,.CL_OUT_D2_I     ()
      ,.CL_OUT_D3_I     ()
      ,.CL_OUT_D4_I     ()
      ,.CL_OUT_D5_I     ()
      ,.CL_OUT_D6_I     ()
      ,.CL_OUT_D7_I     ()
      ,.CL_OUT_D8_I     ()

      // GW -> IC0 (SWIZZLED WIRES)
      ,.CL2_IN_CLK_O    (GW_IC0_CL_CLK)
      ,.CL2_IN_V_O      (GW_IC0_CL_D4)
      ,.CL2_IN_TKN_I    (GW_IC0_CL_TKN)
      ,.CL2_IN_D0_O     (GW_IC0_CL_D6)
      ,.CL2_IN_D1_O     (GW_IC0_CL_D5)
      ,.CL2_IN_D2_O     (GW_IC0_CL_D7)
      ,.CL2_IN_D3_O     (GW_IC0_CL_D8)
      ,.CL2_IN_D4_O     (GW_IC0_CL_D3)
      ,.CL2_IN_D5_O     (GW_IC0_CL_V)
      ,.CL2_IN_D6_O     (GW_IC0_CL_D2)
      ,.CL2_IN_D7_O     (GW_IC0_CL_D1)
      ,.CL2_IN_D8_O     (GW_IC0_CL_D0)

      // GW -> IC<n> (SWIZZLED WIRES)
      ,.CL2_OUT_CLK_O   ()
      ,.CL2_OUT_V_O     ()
      ,.CL2_OUT_TKN_I   ()
      ,.CL2_OUT_D0_O    ()
      ,.CL2_OUT_D1_O    ()
      ,.CL2_OUT_D2_O    ()
      ,.CL2_OUT_D3_O    ()
      ,.CL2_OUT_D4_O    ()
      ,.CL2_OUT_D5_O    ()
      ,.CL2_OUT_D6_O    ()
      ,.CL2_OUT_D7_O    ()
      ,.CL2_OUT_D8_O    ()

      // TAG DRIVERS
      ,.TAG_CLK_O       (GW_TAG_CLKO)
      ,.TAG_EN_O        (GW_IC0_TAG_EN)
      ,.TAG_DATA_O      (GW_TAG_DATAO)

      // CLOCK DRIVERS
      ,.CLKA_O          (GW_CLKA)
      ,.CLKB_O          (GW_CLKB)
      ,.CLKC_O          (GW_CLKC)

      // CLOCK SCOPE
      ,.CLK0_I          (IC0_CLKO)
  
      // SELECT DRIVERS
      ,.SEL0_O          (GW_SEL0)
      ,.SEL1_O          (GW_SEL1)
      ,.SEL2_O          (GW_IC1_TAG_EN)

      // RESET DRIVERS
      ,.CLK_RESET_O     (GW_CLK_RESET)
      ,.CORE_RESET_O    (GW_CORE_RESET)
      );

  //
  // ASIC SOCKET (IC0)
  //

  bsg_asic_socket
    IC0
      // IC1 -> IC0
      (.CL_IN_CLK_I       (IC1_IC0_CL_CLK)
      ,.CL_IN_V_I         (IC1_IC0_CL_V)
      ,.CL_IN_TKN_O       (IC1_IC0_CL_TKN)
      ,.CL_IN_D0_I        (IC1_IC0_CL_D0)
      ,.CL_IN_D1_I        (IC1_IC0_CL_D1)
      ,.CL_IN_D2_I        (IC1_IC0_CL_D2)
      ,.CL_IN_D3_I        (IC1_IC0_CL_D3)
      ,.CL_IN_D4_I        (IC1_IC0_CL_D4)
      ,.CL_IN_D5_I        (IC1_IC0_CL_D5)
      ,.CL_IN_D6_I        (IC1_IC0_CL_D6)
      ,.CL_IN_D7_I        (IC1_IC0_CL_D7)
      ,.CL_IN_D8_I        (IC1_IC0_CL_D8)

      // GW -> IC0
      ,.CL_OUT_CLK_I      (GW_IC0_CL_CLK)
      ,.CL_OUT_V_I        (GW_IC0_CL_V)
      ,.CL_OUT_TKN_O      (GW_IC0_CL_TKN)
      ,.CL_OUT_D0_I       (GW_IC0_CL_D0)
      ,.CL_OUT_D1_I       (GW_IC0_CL_D1)
      ,.CL_OUT_D2_I       (GW_IC0_CL_D2)
      ,.CL_OUT_D3_I       (GW_IC0_CL_D3)
      ,.CL_OUT_D4_I       (GW_IC0_CL_D4)
      ,.CL_OUT_D5_I       (GW_IC0_CL_D5)
      ,.CL_OUT_D6_I       (GW_IC0_CL_D6)
      ,.CL_OUT_D7_I       (GW_IC0_CL_D7)
      ,.CL_OUT_D8_I       (GW_IC0_CL_D8)

      // IC0 -> IC1 (SWIZZLED WIRES)
      ,.CL2_IN_CLK_O      (IC0_IC1_CL_CLK)
      ,.CL2_IN_V_O        (IC0_IC1_CL_D4)
      ,.CL2_IN_TKN_I      (IC0_IC1_CL_TKN)
      ,.CL2_IN_D0_O       (IC0_IC1_CL_D6)
      ,.CL2_IN_D1_O       (IC0_IC1_CL_D5)
      ,.CL2_IN_D2_O       (IC0_IC1_CL_D7)
      ,.CL2_IN_D3_O       (IC0_IC1_CL_D8)
      ,.CL2_IN_D4_O       (IC0_IC1_CL_D3)
      ,.CL2_IN_D5_O       (IC0_IC1_CL_V)
      ,.CL2_IN_D6_O       (IC0_IC1_CL_D2)
      ,.CL2_IN_D7_O       (IC0_IC1_CL_D1)
      ,.CL2_IN_D8_O       (IC0_IC1_CL_D0)

      // IC0 -> GW (SWIZZLED WIRES)
      ,.CL2_OUT_CLK_O     (IC0_GW_CL_CLK)
      ,.CL2_OUT_V_O       (IC0_GW_CL_D4)
      ,.CL2_OUT_TKN_I     (IC0_GW_CL_TKN)
      ,.CL2_OUT_D0_O      (IC0_GW_CL_D8)
      ,.CL2_OUT_D1_O      (IC0_GW_CL_D7)
      ,.CL2_OUT_D2_O      (IC0_GW_CL_V)
      ,.CL2_OUT_D3_O      (IC0_GW_CL_D6)
      ,.CL2_OUT_D4_O      (IC0_GW_CL_D5)
      ,.CL2_OUT_D5_O      (IC0_GW_CL_D3)
      ,.CL2_OUT_D6_O      (IC0_GW_CL_D2)
      ,.CL2_OUT_D7_O      (IC0_GW_CL_D1)
      ,.CL2_OUT_D8_O      (IC0_GW_CL_D0)

      // DDR
      ,.DDR_CK_P_O        (IC0_DDR_CK_P)
      ,.DDR_CK_N_O        (IC0_DDR_CK_N)

      ,.DDR_CKE_O         (IC0_DDR_CKE)

      ,.DDR_CS_N_O        (IC0_DDR_CS_N)
      ,.DDR_RAS_N_O       (IC0_DDR_RAS_N)
      ,.DDR_CAS_N_O       (IC0_DDR_CAS_N)
      ,.DDR_WE_N_O        (IC0_DDR_WE_N)

      ,.DDR_RESET_N_O     (IC0_DDR_RESET_N)
      ,.DDR_ODT_O         (IC0_DDR_ODT)

      ,.DDR_BA0_O         (IC0_DDR_BA0)
      ,.DDR_BA1_O         (IC0_DDR_BA1)
      ,.DDR_BA2_O         (IC0_DDR_BA2)

      ,.DDR_ADDR0_O       (IC0_DDR_ADDR0)
      ,.DDR_ADDR1_O       (IC0_DDR_ADDR1)
      ,.DDR_ADDR2_O       (IC0_DDR_ADDR2)
      ,.DDR_ADDR3_O       (IC0_DDR_ADDR3)
      ,.DDR_ADDR4_O       (IC0_DDR_ADDR4)
      ,.DDR_ADDR5_O       (IC0_DDR_ADDR5)
      ,.DDR_ADDR6_O       (IC0_DDR_ADDR6)
      ,.DDR_ADDR7_O       (IC0_DDR_ADDR7)
      ,.DDR_ADDR8_O       (IC0_DDR_ADDR8)
      ,.DDR_ADDR9_O       (IC0_DDR_ADDR9)
      ,.DDR_ADDR10_O      (IC0_DDR_ADDR10)
      ,.DDR_ADDR11_O      (IC0_DDR_ADDR11)
      ,.DDR_ADDR12_O      (IC0_DDR_ADDR12)
      ,.DDR_ADDR13_O      (IC0_DDR_ADDR13)
      ,.DDR_ADDR14_O      (IC0_DDR_ADDR14)
      ,.DDR_ADDR15_O      (IC0_DDR_ADDR15)

      ,.DDR_DM0_O         (IC0_DDR_DM0)
      ,.DDR_DM1_O         (IC0_DDR_DM1)
      ,.DDR_DM2_O         (IC0_DDR_DM2)
      ,.DDR_DM3_O         (IC0_DDR_DM3)

      ,.DDR_DQS0_P_IO     (IC0_DDR_DQS0_P)
      ,.DDR_DQS0_N_IO     (IC0_DDR_DQS0_N)
      ,.DDR_DQS1_P_IO     (IC0_DDR_DQS1_P)
      ,.DDR_DQS1_N_IO     (IC0_DDR_DQS1_N)
      ,.DDR_DQS2_P_IO     (IC0_DDR_DQS2_P)
      ,.DDR_DQS2_N_IO     (IC0_DDR_DQS2_N)
      ,.DDR_DQS3_P_IO     (IC0_DDR_DQS3_P)
      ,.DDR_DQS3_N_IO     (IC0_DDR_DQS3_N)

      ,.DDR_DQ0_IO        (IC0_DDR_DQ0)
      ,.DDR_DQ1_IO        (IC0_DDR_DQ1)
      ,.DDR_DQ2_IO        (IC0_DDR_DQ2)
      ,.DDR_DQ3_IO        (IC0_DDR_DQ3)
      ,.DDR_DQ4_IO        (IC0_DDR_DQ4)
      ,.DDR_DQ5_IO        (IC0_DDR_DQ5)
      ,.DDR_DQ6_IO        (IC0_DDR_DQ6)
      ,.DDR_DQ7_IO        (IC0_DDR_DQ7)
      ,.DDR_DQ8_IO        (IC0_DDR_DQ8)
      ,.DDR_DQ9_IO        (IC0_DDR_DQ9)
      ,.DDR_DQ10_IO       (IC0_DDR_DQ10)
      ,.DDR_DQ11_IO       (IC0_DDR_DQ11)
      ,.DDR_DQ12_IO       (IC0_DDR_DQ12)
      ,.DDR_DQ13_IO       (IC0_DDR_DQ13)
      ,.DDR_DQ14_IO       (IC0_DDR_DQ14)
      ,.DDR_DQ15_IO       (IC0_DDR_DQ15)
      ,.DDR_DQ16_IO       (IC0_DDR_DQ16)
      ,.DDR_DQ17_IO       (IC0_DDR_DQ17)
      ,.DDR_DQ18_IO       (IC0_DDR_DQ18)
      ,.DDR_DQ19_IO       (IC0_DDR_DQ19)
      ,.DDR_DQ20_IO       (IC0_DDR_DQ20)
      ,.DDR_DQ21_IO       (IC0_DDR_DQ21)
      ,.DDR_DQ22_IO       (IC0_DDR_DQ22)
      ,.DDR_DQ23_IO       (IC0_DDR_DQ23)
      ,.DDR_DQ24_IO       (IC0_DDR_DQ24)
      ,.DDR_DQ25_IO       (IC0_DDR_DQ25)
      ,.DDR_DQ26_IO       (IC0_DDR_DQ26)
      ,.DDR_DQ27_IO       (IC0_DDR_DQ27)
      ,.DDR_DQ28_IO       (IC0_DDR_DQ28)
      ,.DDR_DQ29_IO       (IC0_DDR_DQ29)
      ,.DDR_DQ30_IO       (IC0_DDR_DQ30)
      ,.DDR_DQ31_IO       (IC0_DDR_DQ31)

      // TAG
      ,.TAG_CLK_I         (GW_TAG_CLKO)
      ,.TAG_EN_I          (GW_IC0_TAG_EN)
      ,.TAG_DATA_I        (GW_TAG_DATAO)
      ,.TAG_CLK_O         ()
      ,.TAG_DATA_O        ()

      // CLOCKS
      ,.CLKA_I            (GW_CLKA)
      ,.CLKB_I            (GW_CLKB)
      ,.CLKC_I            (GW_CLKC)

      // CLOCK SCOPE
      ,.CLK_O             (IC0_CLKO)

      // SELECTS
      ,.SEL0_I            (GW_SEL0)
      ,.SEL1_I            (GW_SEL1)
      ,.SEL2_I            ()

      // RESETS
      ,.CLK_RESET_I       (GW_CLK_RESET)
      ,.CORE_RESET_I      (GW_CORE_RESET)

      // MISC
      ,.MISC_O            ()
      );
      
`ifdef MULTI_ASIC
  //
  // ASIC SOCKET (IC1)
  //

  bsg_asic_socket
    IC1
      // IC2 -> IC1
      (.CL_IN_CLK_I       ()
      ,.CL_IN_V_I         ()
      ,.CL_IN_TKN_O       ()
      ,.CL_IN_D0_I        ()
      ,.CL_IN_D1_I        ()
      ,.CL_IN_D2_I        ()
      ,.CL_IN_D3_I        ()
      ,.CL_IN_D4_I        ()
      ,.CL_IN_D5_I        ()
      ,.CL_IN_D6_I        ()
      ,.CL_IN_D7_I        ()
      ,.CL_IN_D8_I        ()

      // IC0 -> IC1
      ,.CL_OUT_CLK_I      (IC0_IC1_CL_CLK)
      ,.CL_OUT_V_I        (IC0_IC1_CL_V)
      ,.CL_OUT_TKN_O      (IC0_IC1_CL_TKN)
      ,.CL_OUT_D0_I       (IC0_IC1_CL_D0)
      ,.CL_OUT_D1_I       (IC0_IC1_CL_D1)
      ,.CL_OUT_D2_I       (IC0_IC1_CL_D2)
      ,.CL_OUT_D3_I       (IC0_IC1_CL_D3)
      ,.CL_OUT_D4_I       (IC0_IC1_CL_D4)
      ,.CL_OUT_D5_I       (IC0_IC1_CL_D5)
      ,.CL_OUT_D6_I       (IC0_IC1_CL_D6)
      ,.CL_OUT_D7_I       (IC0_IC1_CL_D7)
      ,.CL_OUT_D8_I       (IC0_IC1_CL_D8)

      // IC1 -> IC2 (SWIZZLED WIRES)
      ,.CL2_IN_CLK_O      ()
      ,.CL2_IN_V_O        ()
      ,.CL2_IN_TKN_I      ()
      ,.CL2_IN_D0_O       ()
      ,.CL2_IN_D1_O       ()
      ,.CL2_IN_D2_O       ()
      ,.CL2_IN_D3_O       ()
      ,.CL2_IN_D4_O       ()
      ,.CL2_IN_D5_O       ()
      ,.CL2_IN_D6_O       ()
      ,.CL2_IN_D7_O       ()
      ,.CL2_IN_D8_O       ()

      // IC1 -> IC0 (SWIZZLED WIRES)
      ,.CL2_OUT_CLK_O     (IC1_IC0_CL_CLK)
      ,.CL2_OUT_V_O       (IC1_IC0_CL_D4)
      ,.CL2_OUT_TKN_I     (IC1_IC0_CL_TKN)
      ,.CL2_OUT_D0_O      (IC1_IC0_CL_D8)
      ,.CL2_OUT_D1_O      (IC1_IC0_CL_D7)
      ,.CL2_OUT_D2_O      (IC1_IC0_CL_V)
      ,.CL2_OUT_D3_O      (IC1_IC0_CL_D6)
      ,.CL2_OUT_D4_O      (IC1_IC0_CL_D5)
      ,.CL2_OUT_D5_O      (IC1_IC0_CL_D3)
      ,.CL2_OUT_D6_O      (IC1_IC0_CL_D2)
      ,.CL2_OUT_D7_O      (IC1_IC0_CL_D1)
      ,.CL2_OUT_D8_O      (IC1_IC0_CL_D0)

      // DDR
      ,.DDR_CK_P_O        ()
      ,.DDR_CK_N_O        ()

      ,.DDR_CKE_O         ()

      ,.DDR_CS_N_O        ()
      ,.DDR_RAS_N_O       ()
      ,.DDR_CAS_N_O       ()
      ,.DDR_WE_N_O        ()

      ,.DDR_RESET_N_O     ()
      ,.DDR_ODT_O         ()

      ,.DDR_BA0_O         ()
      ,.DDR_BA1_O         ()
      ,.DDR_BA2_O         ()

      ,.DDR_ADDR0_O       ()
      ,.DDR_ADDR1_O       ()
      ,.DDR_ADDR2_O       ()
      ,.DDR_ADDR3_O       ()
      ,.DDR_ADDR4_O       ()
      ,.DDR_ADDR5_O       ()
      ,.DDR_ADDR6_O       ()
      ,.DDR_ADDR7_O       ()
      ,.DDR_ADDR8_O       ()
      ,.DDR_ADDR9_O       ()
      ,.DDR_ADDR10_O      ()
      ,.DDR_ADDR11_O      ()
      ,.DDR_ADDR12_O      ()
      ,.DDR_ADDR13_O      ()
      ,.DDR_ADDR14_O      ()
      ,.DDR_ADDR15_O      ()

      ,.DDR_DM0_O         ()
      ,.DDR_DM1_O         ()
      ,.DDR_DM2_O         ()
      ,.DDR_DM3_O         ()

      ,.DDR_DQS0_P_IO     ()
      ,.DDR_DQS0_N_IO     ()
      ,.DDR_DQS1_P_IO     ()
      ,.DDR_DQS1_N_IO     ()
      ,.DDR_DQS2_P_IO     ()
      ,.DDR_DQS2_N_IO     ()
      ,.DDR_DQS3_P_IO     ()
      ,.DDR_DQS3_N_IO     ()

      ,.DDR_DQ0_IO        ()
      ,.DDR_DQ1_IO        ()
      ,.DDR_DQ2_IO        ()
      ,.DDR_DQ3_IO        ()
      ,.DDR_DQ4_IO        ()
      ,.DDR_DQ5_IO        ()
      ,.DDR_DQ6_IO        ()
      ,.DDR_DQ7_IO        ()
      ,.DDR_DQ8_IO        ()
      ,.DDR_DQ9_IO        ()
      ,.DDR_DQ10_IO       ()
      ,.DDR_DQ11_IO       ()
      ,.DDR_DQ12_IO       ()
      ,.DDR_DQ13_IO       ()
      ,.DDR_DQ14_IO       ()
      ,.DDR_DQ15_IO       ()
      ,.DDR_DQ16_IO       ()
      ,.DDR_DQ17_IO       ()
      ,.DDR_DQ18_IO       ()
      ,.DDR_DQ19_IO       ()
      ,.DDR_DQ20_IO       ()
      ,.DDR_DQ21_IO       ()
      ,.DDR_DQ22_IO       ()
      ,.DDR_DQ23_IO       ()
      ,.DDR_DQ24_IO       ()
      ,.DDR_DQ25_IO       ()
      ,.DDR_DQ26_IO       ()
      ,.DDR_DQ27_IO       ()
      ,.DDR_DQ28_IO       ()
      ,.DDR_DQ29_IO       ()
      ,.DDR_DQ30_IO       ()
      ,.DDR_DQ31_IO       ()

      // TAG
      ,.TAG_CLK_I         (GW_TAG_CLKO)
      ,.TAG_EN_I          (GW_IC1_TAG_EN)
      ,.TAG_DATA_I        (GW_TAG_DATAO)
      ,.TAG_CLK_O         ()
      ,.TAG_DATA_O        ()

      // CLOCKS
      ,.CLKA_I            (GW_CLKA)
      ,.CLKB_I            (GW_CLKB)
      ,.CLKC_I            (GW_CLKC)

      // CLOCK SCOPE
      ,.CLK_O             ()

      // SELECTS
      ,.SEL0_I            (GW_SEL0)
      ,.SEL1_I            (GW_SEL1)
      ,.SEL2_I            ()

      // RESETS
      ,.CLK_RESET_I       (GW_CLK_RESET)
      ,.CORE_RESET_I      (GW_CORE_RESET)

      // MISC
      ,.MISC_O            ()
      );
`endif

  //
  // DRAM (IC0)
  //

  mobile_ddr
    IC0_DDR0
      (.Clk     (IC0_DDR_CK_P)
      ,.Clk_n   (IC0_DDR_CK_N)
      ,.Cke     (IC0_DDR_CKE)
      ,.We_n    (IC0_DDR_WE_N)
      ,.Cs_n    (IC0_DDR_CS_N)
      ,.Ras_n   (IC0_DDR_RAS_N)
      ,.Cas_n   (IC0_DDR_CAS_N)
      ,.Addr    ({IC0_DDR_ADDR13, IC0_DDR_ADDR12, IC0_DDR_ADDR11, IC0_DDR_ADDR10, IC0_DDR_ADDR9, IC0_DDR_ADDR8, IC0_DDR_ADDR7, IC0_DDR_ADDR6, IC0_DDR_ADDR5, IC0_DDR_ADDR4, IC0_DDR_ADDR3, IC0_DDR_ADDR2, IC0_DDR_ADDR1, IC0_DDR_ADDR0})
      ,.Ba      ({IC0_DDR_BA1, IC0_DDR_BA0})
      ,.Dq      ({IC0_DDR_DQ15, IC0_DDR_DQ14, IC0_DDR_DQ13, IC0_DDR_DQ12, IC0_DDR_DQ11, IC0_DDR_DQ10, IC0_DDR_DQ9, IC0_DDR_DQ8, IC0_DDR_DQ7, IC0_DDR_DQ6, IC0_DDR_DQ5, IC0_DDR_DQ4, IC0_DDR_DQ3, IC0_DDR_DQ2, IC0_DDR_DQ1, IC0_DDR_DQ0})
      ,.Dqs     ({IC0_DDR_DQS1_P, IC0_DDR_DQS0_P})
      ,.Dm      ({IC0_DDR_DM1, IC0_DDR_DM0})
      );

  mobile_ddr
    IC0_DDR1
      (.Clk     (IC0_DDR_CK_P)
      ,.Clk_n   (IC0_DDR_CK_N)
      ,.Cke     (IC0_DDR_CKE)
      ,.We_n    (IC0_DDR_WE_N)
      ,.Cs_n    (IC0_DDR_CS_N)
      ,.Ras_n   (IC0_DDR_RAS_N)
      ,.Cas_n   (IC0_DDR_CAS_N)
      ,.Addr    ({IC0_DDR_ADDR13, IC0_DDR_ADDR12, IC0_DDR_ADDR11, IC0_DDR_ADDR10, IC0_DDR_ADDR9, IC0_DDR_ADDR8, IC0_DDR_ADDR7, IC0_DDR_ADDR6, IC0_DDR_ADDR5, IC0_DDR_ADDR4, IC0_DDR_ADDR3, IC0_DDR_ADDR2, IC0_DDR_ADDR1, IC0_DDR_ADDR0})
      ,.Ba      ({IC0_DDR_BA1, IC0_DDR_BA0})
      ,.Dq      ({IC0_DDR_DQ31, IC0_DDR_DQ30, IC0_DDR_DQ29, IC0_DDR_DQ28, IC0_DDR_DQ27, IC0_DDR_DQ26, IC0_DDR_DQ25, IC0_DDR_DQ24, IC0_DDR_DQ23, IC0_DDR_DQ22, IC0_DDR_DQ21, IC0_DDR_DQ20, IC0_DDR_DQ19, IC0_DDR_DQ18, IC0_DDR_DQ17, IC0_DDR_DQ16})
      ,.Dqs     ({IC0_DDR_DQS3_P, IC0_DDR_DQS2_P})
      ,.Dm      ({IC0_DDR_DM3, IC0_DDR_DM2})
      );

endmodule
