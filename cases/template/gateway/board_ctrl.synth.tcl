proc pnsynth {} {
  cd F:/DoubleTrouble/DoubleTroubleGateway/board_ctrl
  if { [ catch { xload xmp board_ctrl.xmp } result ] } {
    exit 10
  }
  if { [catch {run netlist} result] } {
    return -1
  }
  return $result
}
if { [catch {pnsynth} result] } {
  exit -1
}
exit $result
