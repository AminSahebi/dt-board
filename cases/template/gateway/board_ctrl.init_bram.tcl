cd F:/DoubleTrouble/DoubleTroubleGateway/board_ctrl
if { [ catch { xload xmp board_ctrl.xmp } result ] } {
  exit 10
}

if { [catch {run init_bram} result] } {
  exit -1
}

exit 0
