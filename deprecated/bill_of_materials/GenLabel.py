#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 UCSD Bespoken Systems Group
# All rights reserved
# Created by Shengye Wang <shengye@ucsd.edu>

def can_be_same_grp(part_a, part_b):
    if (part_a[3] != part_b[3]): # Type
        return False
    if (part_a[4] != part_b[4]): # Value
        return False
    return True

output_buffer = []
sole_buffer = {}

grp_idx = 0

def flush_parts_buffer(parts_buffer):
    global grp_idx
    global output_buffer
    if (len(parts_buffer) == 0):
        return
    grp_idx += 1
    if (parts_buffer[0][4] != '') :
        grp_title = "%s_%s" % (parts_buffer[0][3], parts_buffer[0][4])
    else:
        grp_title = parts_buffer[0][3]
    parts_ref = ""
    for item in parts_buffer:
        parts_ref += item[1] + " "
    output_buffer.append("%d,%s,%s,%d" % (grp_idx, parts_ref, grp_title, len(parts_buffer)))

fp = open("Parts_XY.txt")
lines = fp.readlines()
fp.close()

parts = []

def to_num(name):
    rtn = 0
    for ch in name:
        if (ord(ch) <= ('9') and ord(ch) >= ord('0')):
            rtn = rtn * 10 + ord(ch) - ord('0')
    return rtn

for line in lines:
    if (line.strip() != ''):
        info = line.strip().split('\t')
        if (info[0] != 'Famliy'):
            parts.append(info)

parts.sort(key = lambda x:(x[4], x[3], to_num(x[1]), x[1]))

parts_buffer = []
for part in parts:
    if (len(parts_buffer) == 0):
        parts_buffer.append(part)
        continue
    if (can_be_same_grp(parts_buffer[0], part)):
        parts_buffer.append(part)
    else:
        flush_parts_buffer(parts_buffer)
        parts_buffer = [part]

if (len(parts_buffer)):
    flush_parts_buffer(parts_buffer)

fp = open("Label_Data.csv", 'w')
fp.write('\n'.join(output_buffer))
fp.close()

